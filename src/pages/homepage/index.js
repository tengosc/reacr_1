import React from "react";
import Lista from "../../components/List";

const homepage = ({ title }) => {
  const myList = [
    "Ramen",
    "Kurczak po wietnamsku",
    "Sajgonki",
    "Barszcz z uszkami",
    "Schabowy z ziemniakami :D",
  ];

  return (
    <div>
      <h1>Najlepsze potrawy</h1>
      <p>Oto lista moich ulubionych {title}:</p>
      <ul>
        {myList.map((elem, index) => {
          return (
            <>
              <Lista index={index + 1} name={elem} />
            </>
          );
        })}
      </ul>
    </div>
  );
};

export default homepage;
