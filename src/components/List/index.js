import React from "react";

const Lista = ({ index, name }) => {
  return (
    <>
      <li>
        {index}. Nazwa potrawy: {name}
      </li>
    </>
  );
};

// class Lista extends React.Component {
//   render() {
//     return (
//       <>
//         <li>
//           {index}. Nazwa potrawy: {name}
//         </li>
//       </>
//     );
//   }
// }

export default Lista;
